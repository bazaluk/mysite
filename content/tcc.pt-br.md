### Trabalho de Conclusão de Curso 

## Técnicas Computacionais para a Recuperação de Informação Musical em Repertórios Indígenas Brasileiros
### Orientador: Professor Marcelo Queiroz

Certos estilos musicais apresentam padrões rítmicos característicos que podem ser identificados por qualquer ouvinte. Na transcrição de tais padrões, frequentemente são realizadas aproximações das durações a fim de simplificar a representação simbólica, principalmente no caso da notação musical padrão. Por isso, é frequente encontrarmos pequenas discrepâncias entre a representação simbólica e as realizações musicais, e quando tais discrepâncias são observadas de forma sistemática, é possível investigar certas características do estilo musical, tais como a expressividade, frequentemente inacessíveis através da notação simbólica. A área de estudo que investiga estas questões é denominada microtiming (microtemporalidade) (Gouyon, 2007).
Neste trabalho, faremos uma análise de padrões rítmicos similares seguindo a metodologia de Fabien Gouyon em seu artigo *“Microtiming in ‘Samba de Roda’ — Preliminary experiments with polyphonic audio”* (Gouyon, 2007). O objetivo deste trabalho é investigar duas peças do repertório Parixara, um estilo de música e dança que está presente nas culturas indígenas roraimenses Wapichana e Macuxi (Fernandes, 2015), e analisar as variações em *“microtiming”* dos padrões rítmicos encontrados na partitura. Nossas conclusões foram que a metodologia original tem sucesso bastante limitado neste repertório, que outros caminhos de investigação usando *clusterização* de onsets são plausíveis, e que a sonoridade longa e reiterada do chocalho influenciou os resultados e dificultou a análise.

**Palavras-chave:** música indígena, cultura indígena, macuxi, wapichana, povos originários, computação musical, microtemporalidade, MIR.

[Proposta de projeto]({{< ref "/projeto" >}} "Proposta de projeto")

[Monografia]({{< ref "/monografia" >}} "Monografia")

[Códigos](https://gitlab.com/bazaluk/parixara_analysis/)


